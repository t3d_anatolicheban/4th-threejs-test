import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls.js'
import * as dat from 'lil-gui'
import gsap from 'gsap'
import { AmbientLight, AxesHelper, MeshBasicMaterial, MeshStandardMaterial, PointLight, Raycaster, Vector3 } from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'

/**
 * Base
 */
// Debug
const gui = new dat.GUI()

//Params and Tweaks
const params = {
}

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

//Axes Helper
const axesHelper = new AxesHelper(60)
scene.add(axesHelper)

/**
 * Sizes
 */
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight
}

window.addEventListener('resize', () => {
  // Update sizes
  sizes.width = window.innerWidth
  sizes.height = window.innerHeight

  // Update camera
  camera.aspect = sizes.width / sizes.height
  camera.updateProjectionMatrix()

  // Update renderer
  renderer.setSize(sizes.width, sizes.height)
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 1000)
camera.position.x = 0
camera.position.y = 10
camera.position.z = 0
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

const pointer = new PointerLockControls(camera, document.body)

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
  canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
renderer.setClearColor('#26abff')

//TEST
window.addEventListener('keydown', (e) => {
  // pointer.unlock()
  // console.log(e);
  if (e.code === 'KeyP') {
    document.body.requestPointerLock()
  }
})

let ambLight = new AmbientLight('#fff', .3)

scene.add(ambLight)

let pointLight = new PointLight('#ff0', 1)
pointLight.position.set(0, 10, 0)
scene.add(pointLight)
const loader = new GLTFLoader()

let plane;

loader.load('plane.glb', (gltf) => {
  console.log(gltf);
  plane = gltf.scene
  plane.children[0].material = new MeshStandardMaterial()
  scene.add(plane)
})

let raycaster = new Raycaster()
/**
 * Animate
 */
const clock = new THREE.Clock()

let go = {
  x: 0,
  z: 0
}

const tick = () => {
  const elapsedTime = clock.getElapsedTime()

  // Update controls
  // controls.update()

  if (plane) {
    go.x && (camera.position.x += go.x * .5)
    go.z && (camera.position.z += go.z * .5)
    let origin = new Vector3(camera.position.x, camera.position.y, camera.position.z)

    let dir = new Vector3(
      camera.position.x,
      camera.position.y - 30,
      camera.position.z)
    // console.log(origin, dir);
    raycaster.set(origin, dir)

    const intersect = raycaster.intersectObject(plane)
    if (intersect.length) {
      camera.position.y = intersect[0].point.y + 2
    }
  }


  // Render
  renderer.render(scene, camera)

  // Call tick again on the next frame
  window.requestAnimationFrame(tick)
}

tick()

window.addEventListener('keydown', (e) => {
  let v = new Vector3()
  if (e.code === 'KeyW') {
    pointer.getDirection(v)
    go.x = v.x
    go.z = v.z
  } else if (e.code === 'KeyD') {

  } else if (e.code === 'KeyS') {

  } else if (e.code === 'KeyA') {

  }
})
window.addEventListener('keyup', (e) => {
  if (e.code === 'KeyW' || e.code === 'KeyD' || e.code === 'KeyS' || e.code === 'KeyA') {

    go.x && (go.x = 0)
    go.z && (go.z = 0)
  }
})

setInterval(() => {
  let origin = new Vector3(camera.position.x, camera.position.y, camera.position.z)

  let dir = new Vector3(
    camera.position.x,
    camera.position.y - 30,
    camera.position.z).normalize()
  // console.log(origin, dir);
  raycaster.set(origin, dir)

  const intersect = raycaster.intersectObject(plane)

  console.log(intersect, camera.position.y);
}, 2000)